package com.beveragescost;

import javax.swing.*;

public class beveragescost {
    public static void main(String[] args) {
        String inputValue,checkBeverageType,checkBeverageSize,checkFlavoring;
        double smallCost=1.50,mediumCost=2.50,largeCost=3.25,vanillaCost=0.25,chocolateCost=0.75,mintCost=0.50,lemonCost=0.25,total,tax;
        String customerName = JOptionPane.showInputDialog("Enter customer name:");
        String beverageType = JOptionPane.showInputDialog("Enter type of beverage(tea or coffee):");
        checkBeverageType=beverageType;
        if(checkBeverageType.equalsIgnoreCase("tea")| checkBeverageType.equalsIgnoreCase("t")|checkBeverageType.equalsIgnoreCase("coffee")|checkBeverageType.equalsIgnoreCase("C")) {
            String beverageSize = JOptionPane.showInputDialog("Enter beverage size(small/medium/large):");
            checkBeverageSize = beverageSize;
            if (checkBeverageSize.equalsIgnoreCase("small") | checkBeverageSize.equalsIgnoreCase("medium") | checkBeverageSize.equalsIgnoreCase("large") | checkBeverageSize.equalsIgnoreCase("s") | checkBeverageSize.equalsIgnoreCase("m") | checkBeverageSize.equalsIgnoreCase("L")) {
                if (checkBeverageType.equalsIgnoreCase("coffee") | checkBeverageType.equalsIgnoreCase("c")) {
                    String flavoringCoffee = JOptionPane.showInputDialog("Enter any flavoring(none/vanilla/chocolate):");
                    checkFlavoring = flavoringCoffee;
                    if (checkFlavoring.equalsIgnoreCase("none") | checkFlavoring.equalsIgnoreCase("vanilla") | checkFlavoring.equalsIgnoreCase("chocolate") | checkFlavoring.equalsIgnoreCase("c") | checkFlavoring.equalsIgnoreCase("v")) {
                        if (checkFlavoring.equalsIgnoreCase("vanilla") & checkBeverageSize.equalsIgnoreCase("small") | checkFlavoring.equalsIgnoreCase("vanilla") & checkBeverageSize.equalsIgnoreCase("s") | checkFlavoring.equalsIgnoreCase("v") & checkBeverageSize.equalsIgnoreCase("small") | checkFlavoring.equalsIgnoreCase("v") & checkBeverageSize.equalsIgnoreCase("s")) {
                            tax = 0.13 * (smallCost + vanillaCost);
                            total = tax + smallCost + vanillaCost;
                            JOptionPane.showMessageDialog(null, "For " + customerName + ", a" + " " + beverageSize.toLowerCase() + " " + beverageType.toLowerCase() + ", " + flavoringCoffee.toLowerCase() + ", cost:$" + Math.round(total));
                        } else if (checkFlavoring.equalsIgnoreCase("vanilla") & checkBeverageSize.equalsIgnoreCase("medium") | checkFlavoring.equalsIgnoreCase("vanilla") & checkBeverageSize.equalsIgnoreCase("m") | checkFlavoring.equalsIgnoreCase("v") & checkBeverageSize.equalsIgnoreCase("medium") | checkFlavoring.equalsIgnoreCase("v") & checkBeverageSize.equalsIgnoreCase("m")) {
                            tax = 0.13 * (mediumCost + vanillaCost);
                            total = tax + mediumCost + vanillaCost;
                            JOptionPane.showMessageDialog(null, "For " + customerName + ", a" + " " + beverageSize.toLowerCase() + " " + beverageType.toLowerCase() + ", " + flavoringCoffee.toLowerCase() + ", cost:$" + total);
                        } else if ((checkFlavoring.equalsIgnoreCase("vanilla") | checkFlavoring.equalsIgnoreCase("v")) & (checkBeverageSize.equalsIgnoreCase("large") | checkBeverageSize.equalsIgnoreCase("l"))) {
                            tax = 0.13 * (largeCost + vanillaCost);
                            total = tax + largeCost + vanillaCost;
                            JOptionPane.showMessageDialog(null, "For " + customerName + ", a" + " " + beverageSize.toLowerCase() + " " + beverageType.toLowerCase() + ", " + flavoringCoffee.toLowerCase() + ", cost:$" + total);
                        } else if (checkFlavoring.equalsIgnoreCase("chocolate") & checkBeverageSize.equalsIgnoreCase("small") | checkFlavoring.equalsIgnoreCase("chocolate") & checkBeverageSize.equalsIgnoreCase("s") | checkFlavoring.equalsIgnoreCase("c") & checkBeverageSize.equalsIgnoreCase("small") | checkFlavoring.equalsIgnoreCase("c") & checkBeverageSize.equalsIgnoreCase("s")) {
                            tax = 0.13 * (smallCost + chocolateCost);
                            total = tax + smallCost + chocolateCost;
                            JOptionPane.showMessageDialog(null, "For " + customerName + ", a" + " " + beverageSize.toLowerCase() + " " + beverageType.toLowerCase() + ", " + flavoringCoffee.toLowerCase() + ", cost:$" + total);
                        } else if (checkFlavoring.equalsIgnoreCase("chocolate") & checkBeverageSize.equalsIgnoreCase("medium") | checkFlavoring.equalsIgnoreCase("chocolate") & checkBeverageSize.equalsIgnoreCase("m") | checkFlavoring.equalsIgnoreCase("c") & checkBeverageSize.equalsIgnoreCase("medium") | checkFlavoring.equalsIgnoreCase("c") & checkBeverageSize.equalsIgnoreCase("m")) {
                            tax = 0.13 * (mediumCost + chocolateCost);
                            total = tax + mediumCost + chocolateCost;
                            JOptionPane.showMessageDialog(null, "For " + customerName + ", a" + " " + beverageSize.toLowerCase() + " " + beverageType.toLowerCase() + ", " + flavoringCoffee.toLowerCase() + ", cost:$" + total);
                        } else if ((checkFlavoring.equalsIgnoreCase("chocolate") | checkFlavoring.equalsIgnoreCase("c")) & (checkBeverageSize.equalsIgnoreCase("large") | checkBeverageSize.equalsIgnoreCase("l"))) {
                            tax = 0.13 * (largeCost + chocolateCost);
                            total = tax + largeCost + chocolateCost;
                            JOptionPane.showMessageDialog(null, "For " + customerName + ", a" + " " + beverageSize.toLowerCase() + " " + beverageType.toLowerCase() + ", " + flavoringCoffee.toLowerCase() + ", cost:$" + total);
                        } else if (checkFlavoring.equalsIgnoreCase("none") & checkBeverageSize.equalsIgnoreCase("small") | checkFlavoring.equalsIgnoreCase("none") & checkBeverageSize.equalsIgnoreCase("s")) {
                            tax = 0.13 * (smallCost);
                            total = tax + smallCost;
                            JOptionPane.showMessageDialog(null, "For " + customerName + ", a" + " " + beverageSize.toLowerCase() + " " + beverageType.toLowerCase() + ", " + flavoringCoffee.toLowerCase() + ", cost:$" + total);
                        } else if (checkFlavoring.equalsIgnoreCase("none") & checkBeverageSize.equalsIgnoreCase("medium") | checkFlavoring.equalsIgnoreCase("none") & checkBeverageSize.equalsIgnoreCase("m")) {
                            tax = 0.13 * (mediumCost);
                            total = tax + mediumCost;
                            JOptionPane.showMessageDialog(null, "For " + customerName + ", a" + " " + beverageSize.toLowerCase() + " " + beverageType.toLowerCase() + ", " + flavoringCoffee.toLowerCase() + ", cost:$" + total);
                        } else {
                            tax = 0.13 * (largeCost);
                            total = tax + largeCost;
                            JOptionPane.showMessageDialog(null, "For " + customerName + ", a" + " " + beverageSize.toLowerCase() + " " + beverageType.toLowerCase() + ", " + flavoringCoffee.toLowerCase() + ", cost:$" + total);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Enter a valid flavoring");
                    }
                } else if (checkBeverageType.equalsIgnoreCase("tea") | checkBeverageType.equalsIgnoreCase("t")) {
                    String flavoringTea = JOptionPane.showInputDialog("Enter any tea flavorings(none/lemon/mint):");
                    checkFlavoring = flavoringTea;
                    if (checkFlavoring.equalsIgnoreCase("none") | checkFlavoring.equalsIgnoreCase("lemon") | checkFlavoring.equalsIgnoreCase("mint") | checkFlavoring.equalsIgnoreCase("m") | checkFlavoring.equalsIgnoreCase("l")) {
                        if (checkFlavoring.equalsIgnoreCase("lemon") & checkBeverageSize.equalsIgnoreCase("small") | checkFlavoring.equalsIgnoreCase("lemon") & checkBeverageSize.equalsIgnoreCase("s") | checkFlavoring.equalsIgnoreCase("l") & checkBeverageSize.equalsIgnoreCase("small") | checkFlavoring.equalsIgnoreCase("l") & checkBeverageSize.equalsIgnoreCase("s")) {
                            tax = 0.13 * (smallCost + lemonCost);
                            total = tax + smallCost + lemonCost;
                            JOptionPane.showMessageDialog(null, "For " + customerName + ", a" + " " + beverageSize.toLowerCase() + " " + beverageType.toLowerCase() + ", " + flavoringTea.toLowerCase() + ", cost:$" + total);
                        } else if (checkFlavoring.equalsIgnoreCase("lemon") & checkBeverageSize.equalsIgnoreCase("medium") | checkFlavoring.equalsIgnoreCase("lemon") & checkBeverageSize.equalsIgnoreCase("m") | checkFlavoring.equalsIgnoreCase("l") & checkBeverageSize.equalsIgnoreCase("medium") | checkFlavoring.equalsIgnoreCase("l") & checkBeverageSize.equalsIgnoreCase("m")) {
                            tax = 0.13 * (mediumCost + lemonCost);
                            total = tax + mediumCost + lemonCost;
                            JOptionPane.showMessageDialog(null, "For " + customerName + ", a" + " " + beverageSize.toLowerCase() + " " + beverageType.toLowerCase() + ", " + flavoringTea.toLowerCase() + ", cost:$" + total);
                        } else if (checkFlavoring.equalsIgnoreCase("lemon") & checkBeverageSize.equalsIgnoreCase("large") | checkFlavoring.equalsIgnoreCase("l") & checkBeverageSize.equalsIgnoreCase("large") | (checkFlavoring.equalsIgnoreCase("lemon") | checkFlavoring.equalsIgnoreCase("l")) & checkBeverageSize.equalsIgnoreCase("l")) {
                            tax = 0.13 * (largeCost + lemonCost);
                            total = tax + largeCost + lemonCost;
                            JOptionPane.showMessageDialog(null, "For " + customerName + ", a" + " " + beverageSize.toLowerCase() + " " + beverageType.toLowerCase() + ", " + flavoringTea.toLowerCase() + ", cost:$" + total);
                        } else if (checkFlavoring.equalsIgnoreCase("mint") & checkBeverageSize.equalsIgnoreCase("small") | checkFlavoring.equalsIgnoreCase("mint") & checkBeverageSize.equalsIgnoreCase("s") | checkFlavoring.equalsIgnoreCase("m") & checkBeverageSize.equalsIgnoreCase("small") | checkFlavoring.equalsIgnoreCase("m") & checkBeverageSize.equalsIgnoreCase("s")) {
                            tax = 0.13 * (smallCost + mintCost);
                            total = tax + smallCost + mintCost;
                            JOptionPane.showMessageDialog(null, "For " + customerName + ", a" + " " + beverageSize.toLowerCase() + " " + beverageType.toLowerCase() + ", " + flavoringTea.toLowerCase() + ", cost:$" + total);
                        } else if (checkFlavoring.equalsIgnoreCase("mint") & checkBeverageSize.equalsIgnoreCase("medium") | checkFlavoring.equalsIgnoreCase("mint") & checkBeverageSize.equalsIgnoreCase("m") | checkFlavoring.equalsIgnoreCase("m") & (checkBeverageSize.equalsIgnoreCase("medium") | checkBeverageSize.equalsIgnoreCase("m"))) {
                            tax = 0.13 * (mediumCost + mintCost);
                            total = tax + mediumCost + mintCost;
                            JOptionPane.showMessageDialog(null, "For " + customerName + ", a" + " " + beverageSize.toLowerCase() + " " + beverageType.toLowerCase() + ", " + flavoringTea.toLowerCase() + ", cost:$" + total);
                        } else if (checkFlavoring.equalsIgnoreCase("mint") & checkBeverageSize.equalsIgnoreCase("large") | checkFlavoring.equalsIgnoreCase("m") & checkBeverageSize.equalsIgnoreCase("large") | (checkFlavoring.equalsIgnoreCase("mint") | checkFlavoring.equalsIgnoreCase("m")) & checkBeverageSize.equalsIgnoreCase("l")) {
                            tax = 0.13 * (largeCost + mintCost);
                            total = tax + largeCost + mintCost;
                            JOptionPane.showMessageDialog(null, "For " + customerName + ", a" + " " + beverageSize.toLowerCase() + " " + beverageType.toLowerCase() + ", " + flavoringTea.toLowerCase() + ", cost:$" + total);
                        } else if (checkFlavoring.equalsIgnoreCase("none") & (checkBeverageSize.equalsIgnoreCase("small") | checkBeverageSize.equalsIgnoreCase("s"))) {
                            tax = 0.13 * (smallCost);
                            total = tax + smallCost;
                            JOptionPane.showMessageDialog(null, "For " + customerName + ", a" + " " + beverageSize.toLowerCase() + " " + beverageType.toLowerCase() + ", " + flavoringTea.toLowerCase() + ", cost:$" + total);
                        } else {
                            JOptionPane.showMessageDialog(null, "Enter a valid flavoring");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Enter either of 2 beverages:tea or coffee.");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Enter either of the 3 valid sizes: small/medium/large");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Enter either of 2 beverages:tea or coffee.");
            }
        }
    }
}
